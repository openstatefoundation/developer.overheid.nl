from django.contrib import admin
from .models import Badge, APIBadge

# Register your models here.
admin.site.register(Badge)
admin.site.register(APIBadge)
