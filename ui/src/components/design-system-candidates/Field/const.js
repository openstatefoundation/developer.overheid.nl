// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
export const WIDTH = {
  SMALL: 'small',
  MEDIUM: 'medium',
  LARGE: 'large',
}
