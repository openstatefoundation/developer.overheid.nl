image: docker:19.03.1

stages:
  - validate
  - test
  - release
  - review
  - deploy
  # - e2e-tests

variables:
  IMAGE_PREFIX: $CI_REGISTRY_IMAGE
  DOMAIN: developer.overheid.nl
  REVIEW_BASE_DOMAIN: nlx.reviews
  REVIEW_NAMESPACE: "don-$CI_ENVIRONMENT_SLUG"

.validate:
  image: golang:1.13.5
  before_script:
    - cd validate
  tags:
    - docker
    - linux

Test validate:
  extends: .validate
  stage: test
  script:
    - go test ./... -coverprofile coverage.out
    - go tool cover -html=coverage.out -o coverage.html
    - go tool cover -func=coverage.out
  coverage: /total:\t+\(statements\)\t+([\d\.]+?%)/
  artifacts:
    expire_in: 1 month
    paths:
      - validate/coverage.html

Validate API definitions:
  extends: .validate
  stage: validate
  script:
    - go run ./cmd/don-validate/main.go

Lint and test API:
  image: python:3.8
  stage: test
  services:
    - postgres:11.6-alpine
  variables:
    PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
    DB_HOST: postgres
    DB_USER: postgres
    DB_PASSWORD: postgres
    DB_NAME: postgres
    SECRET_KEY: somethingverysecret
  cache:
    paths:
      - $CI_PROJECT_DIR/.cache/pip
  before_script:
    - cd api/
    - pip3 install -r requirements-dev.txt
  script:
    - python3 -m prospector
    - python3 -m coverage run --source='.' manage.py test core
    - python3 -m coverage report && python -m coverage html
  coverage: '/TOTAL.+ ([0-9]{1,3}%)/'
  artifacts:
    expire_in: 1 month
    paths:
      - api/htmlcov
  tags:
    - docker
    - linux

## UI ##
.ui:
  before_script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - export UI_IMAGE=${IMAGE_PREFIX}/ui
  tags:
    - docker
    - linux
  variables:
    DOCKER_TLS_CERTDIR: "/certs"
  services:
    - docker:19.03.1-dind

Test and release UI:
  extends: .ui
  stage: release
  script:
    - |
        docker build \
        --tag $UI_IMAGE:$CI_COMMIT_SHORT_SHA \
        --tag $UI_IMAGE:$CI_COMMIT_REF_SLUG \
        -f ui/Dockerfile . && \
        docker push $UI_IMAGE:$CI_COMMIT_SHORT_SHA && \
        docker push $UI_IMAGE:$CI_COMMIT_REF_SLUG
  coverage: /All\sfiles.*?\s+(\d+.\d+)/

## API ##
.api:
  before_script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - export API_IMAGE=${IMAGE_PREFIX}/api
  tags:
    - docker
    - linux
  variables:
    DOCKER_TLS_CERTDIR: "/certs"
  services:
    - docker:19.03.1-dind

Release API:
  extends: .api
  stage: release
  script:
    - |
        docker build \
        --tag $API_IMAGE:$CI_COMMIT_SHORT_SHA \
        --tag $API_IMAGE:$CI_COMMIT_REF_SLUG \
        -f api/Dockerfile . && \
        docker push $API_IMAGE:$CI_COMMIT_SHORT_SHA && \
        docker push $API_IMAGE:$CI_COMMIT_REF_SLUG
  coverage: /total:\t+\(statements\)\t+([\d\.]+?%)/

## Docs ##
.docs:
  before_script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - export DOCS_IMAGE=${IMAGE_PREFIX}/docs
  tags:
    - docker
    - linux
  variables:
    DOCKER_TLS_CERTDIR: "/certs"
  services:
    - docker:19.03.1-dind

Release Docs:
  extends: .docs
  stage: release
  script:
    - |
        docker build \
        --tag $DOCS_IMAGE:$CI_COMMIT_SHORT_SHA \
        --tag $DOCS_IMAGE:$CI_COMMIT_REF_SLUG \
        -f docs/Dockerfile . && \
        docker push $DOCS_IMAGE:$CI_COMMIT_SHORT_SHA && \
        docker push $DOCS_IMAGE:$CI_COMMIT_REF_SLUG

## Review App ##
Deploy Review App:
  stage: review
  image: registry.gitlab.com/commonground/core/review-app-deployer:latest
  variables:
    KUBECONFIG: /secrets/kubeconfig/review.conf
  script:
    - echo -e -n "https://don-$CI_ENVIRONMENT_SLUG.$REVIEW_BASE_DOMAIN" > ci_environment_url.txt
    - |
        kubectl create namespace $REVIEW_NAMESPACE || true
        helm upgrade --install $REVIEW_NAMESPACE ./helm/don \
        --namespace $REVIEW_NAMESPACE \
        --set "secretKey=$SECRET_KEY" \
        --set-string "gitlabProjectId=$GITLAB_PROJECT_ID" \
        --set "gitlabUrl=$GITLAB_URL" \
        --set "gitlabAccessToken=$GITLAB_ACCESS_TOKEN" \
        --set "apiImage=$IMAGE_PREFIX/api:$CI_COMMIT_SHORT_SHA" \
        --set "uiImage=$IMAGE_PREFIX/ui:$CI_COMMIT_SHORT_SHA" \
        --set "docsImage=$IMAGE_PREFIX/docs:$CI_COMMIT_SHORT_SHA" \
        --set "domain=don-$CI_ENVIRONMENT_SLUG.$REVIEW_BASE_DOMAIN" \
        --set "storageType=Ephemeral" \
        --set "loadTestData=true" \
        --set "testAdminPassword=$TEST_ADMIN_PASSWORD" \
        --set "replicas=1"
  environment:
    name: $CI_COMMIT_REF_NAME
    url: https://don-$CI_ENVIRONMENT_SLUG.$REVIEW_BASE_DOMAIN
    on_stop: Remove Review App
  tags:
    - cg
    - docker
  only:
    - branches@commonground/developer.overheid.nl
  except:
    - master
  artifacts:
    paths:
      - ci_environment_url.txt

Remove Review App:
  stage: review
  image: registry.gitlab.com/commonground/core/review-app-deployer:latest
  variables:
    GIT_STRATEGY: none
    KUBECONFIG: /secrets/kubeconfig/review.conf
  script:
    - helm uninstall $REVIEW_NAMESPACE --namespace $REVIEW_NAMESPACE
    - kubectl delete namespace $REVIEW_NAMESPACE
  when: manual
  environment:
    name: $CI_COMMIT_REF_NAME
    url: https://don-$CI_ENVIRONMENT_SLUG.$REVIEW_BASE_DOMAIN
    action: stop
  tags:
    - cg
    - docker
  only:
    - branches@commonground/developer.overheid.nl
  except:
    - master

## E2E Tests
# E2E tests:
#   stage: e2e-tests
#   before_script:
#     - export URL=$([ -f ci_environment_url.txt ] && cat ci_environment_url.txt || echo "https://$DOMAIN")
#     - export E2E_IMAGE=${IMAGE_PREFIX}/e2e
#   script:
#     - |
#       docker build --tag $E2E_IMAGE:$CI_COMMIT_SHORT_SHA \
#       -f e2e-tests/Dockerfile . && \
#       docker run --rm --cap-add=SYS_ADMIN \
#       -e URL=${URL} \
#       $E2E_IMAGE:$CI_COMMIT_SHORT_SHA \
#       /bin/sh -c "./wait-for-http.sh $URL && npm test"
#   tags:
#     - docker
#     - linux
#   only:
#     - branches@commonground/developer.overheid.nl
#   variables:
#     DOCKER_TLS_CERTDIR: "/certs"
#   services:
#     - docker:19.03.1-dind

## Depoyment ##
Deploy production:
  stage: deploy
  image: registry.gitlab.com/commonground/core/review-app-deployer:latest
  variables:
    KUBECONFIG: /secrets/kubeconfig/dv-core-prod.conf
  script:
    - |
        helm upgrade --install don-prod ./helm/don \
        --namespace don-prod \
        --set "secretKey=$SECRET_KEY" \
        --set-string "gitlabProjectId=$GITLAB_PROJECT_ID" \
        --set "gitlabUrl=$GITLAB_URL" \
        --set "gitlabAccessToken=$GITLAB_ACCESS_TOKEN" \
        --set "apiImage=$IMAGE_PREFIX/api:$CI_COMMIT_SHORT_SHA" \
        --set "uiImage=$IMAGE_PREFIX/ui:$CI_COMMIT_SHORT_SHA" \
        --set "docsImage=$IMAGE_PREFIX/docs:$CI_COMMIT_SHORT_SHA" \
        --set "domain=$DOMAIN"
  environment:
    name: prod
    url: https://$DOMAIN
  tags:
    - cg-privileged
    - docker
  only:
    - master@commonground/developer.overheid.nl
